require './Libraries/SyntacticAnalizer/LR1_Analizer'
require './Libraries/SyntacticAnalizer/LL_Analizer'
require './Libraries/SyntacticAnalizer/SLR'
require './Libraries/SyntacticAnalizer/RE_SyntacticAnalizer'
require './Libraries/SyntacticAnalizer/GG_SyntacticAnalizer'
require 'java'

import java.awt.GridLayout
import java.awt.Dimension
import java.awt.Color
import javax.swing.JButton
import javax.swing.SwingConstants
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JTextArea
import javax.swing.JScrollPane
import javax.swing.JEditorPane
import javax.swing.BorderFactory
import javax.swing.GroupLayout
import javax.swing.JTextField
import javax.swing.JOptionPane
import javax.swing.JPanel
import javax.swing.BoxLayout
import java.awt.FlowLayout
import javax.swing.JFileChooser
import javax.swing.filechooser::FileNameExtensionFilter
import java.awt.event.ActionListener
import java.awt.event.KeyEvent
import javax.swing.JMenuBar
import javax.swing.JMenu
import javax.swing.JMenuItem
import javax.swing.KeyStroke

class Menu < JFrame
  
  def initialize
      super "Analyzers"
      self.iniComponents
      self.buttonEvents     #button events
      $grammarType = 3     #default initialize analizer
      self.componentsInitializing
      $grammarLabel.setText "Grammar        LL(1)"
  end
    
  def iniComponents 
      $grammarLabel = JLabel.new "Grammar"
      $type = JLabel.new ""
      $showGrammar = JTextArea.new 
      $expresion = JTextField.new 15 
      $read = JButton.new "Read Grammar "    
      $load = JButton.new "Load Grammar"    
      $analyse = JButton.new "Analyse expresion"
      $menubar = JMenuBar.new
      $fileMenu = JMenu.new "File"
      $about = JMenu.new "About"
      $analizer = JMenu.new "Analizers"
      $ll = JMenuItem.new "LL(1)"
      $lr0 = JMenuItem.new "LR(0)"
      $lr1 = JMenuItem.new "LR(1)"
      $lslr = JMenuItem.new "LALR(0)"
      $clean = JMenuItem.new "Clean"
  end

def componentsInitializing
    #buttons
    $load.setEnabled false
    $analyse.setEnabled false
    $expresion.setEnabled false

    #fields
    $showGrammar.setText ""
    $expresion.setText ""

    #components
    $showGrammar.setPreferredSize Dimension.new(150, 200)
    $showGrammar.setEditable false
    $showGrammar.setBorder BorderFactory.createLineBorder Color.gray
    
    layout = GroupLayout.new self.getContentPane
    self.getContentPane.setLayout layout
    layout.setAutoCreateGaps true
    layout.setAutoCreateContainerGaps true

    #panel
    sg = layout.createSequentialGroup
    pg1 = layout.createParallelGroup
    pg2 = layout.createParallelGroup
    pg1.addComponent $grammarLabel
    pg1.addComponent $showGrammar
    pg1.addComponent $expresion
    sg.addGroup pg1
    pg2.addComponent $read
    pg2.addComponent $load      
    pg2.addComponent $analyse
    sg.addGroup pg2
    layout.setHorizontalGroup sg
      
    sg1 = layout.createSequentialGroup
    sg2 = layout.createSequentialGroup
    pg1 = layout.createParallelGroup
    pg2 = layout.createParallelGroup      
    sg1.addComponent $grammarLabel
    pg1.addComponent $showGrammar
    sg2.addComponent $read
    sg2.addComponent $load
    pg1.addGroup sg2
    sg1.addGroup pg1
    pg2.addComponent $expresion
    pg2.addComponent $analyse
    sg1.addGroup pg2
    layout.setVerticalGroup sg1

    #menu 
    $fileMenu.setMnemonic KeyEvent::VK_F
    $about.setMnemonic KeyEvent::VK_F
    $analizer.setMnemonic KeyEvent::VK_M
    $analizer.add $ll
    $analizer.add $lr0
    $analizer.add $lr1
    $analizer.add $lslr
    $clean.setMnemonic KeyEvent::VK_N
    $fileMenu.add $analizer
    $fileMenu.add $clean
    $menubar.add $fileMenu
    #$menubar.add $about
    self.setJMenuBar $menubar
      
    #frame
    layout.linkSize SwingConstants::HORIZONTAL, 
    $analyse, $expresion, $load, $read

    self.pack
    self.setDefaultCloseOperation JFrame::EXIT_ON_CLOSE
    self.setLocationRelativeTo nil
    self.setVisible true
end

def buttonEvents 
     $read.add_action_listener Read_grammar.new
	 self.pack
     $load.add_action_listener Load_grammar.new
     $analyse.add_action_listener Analize_grammar.new

     $ll.addActionListener do |e| 
       self.componentsInitializing
       $grammarLabel.setText "Grammar        LL(1)"
       $grammarType = 1
     end
     
     $lr0.addActionListener do |e| 
       self.componentsInitializing
       $grammarLabel.setText "Grammar        LR(0)"
       $grammarType = 2
     end
     
     $lr1.addActionListener do |e| 
       self.componentsInitializing
      $grammarLabel.setText "Grammar        LR(1)"
       $grammarType = 3
     end
     
     $lslr.addActionListener do |e| 
       self.componentsInitializing
       $grammarLabel.setText "Grammar        LALR(1)"
       $grammarType = 4
     end
     
     $clean.addActionListener do |e| 
       self.componentsInitializing
     end
end 
  
end

class Read_grammar < JFrame 
  include java.awt.event.ActionListener

  def actionPerformed(evt)
    fileChooser = JFileChooser.new
    filter = FileNameExtensionFilter.new ".txt","txt"
    fileChooser.setFileFilter filter
    result = fileChooser.showOpenDialog nil

    if result == JFileChooser::APPROVE_OPTION
      file = fileChooser.getSelectedFile
      filename = file.getCanonicalPath
      f = File.open filename, "r"
      $text = f.read
      $showGrammar.setText nil
      $showGrammar.setText $text.to_s
      $load.setEnabled true
    end
  end
end


class Load_grammar
  include java.awt.event.ActionListener
  
  def actionPerformed(evt)
    grammar_to_analyse = GG_SyntacticAnalizer.new($text).analize
    token = 0
    sets = Array.new 
    simplify = Array.new
    $tmp_array = Array.new
    $la = LexicalAnalizer.new "",""
    re_sa = RE_SyntacticAnalizer.new "(#{RE_Tokens::SCAPE_CHAR}  OR \n OR \t)+" #detecting blank spaces, break lines, tabs  
    $la.addAnalizer AnalizerNFA.new( re_sa.create_NFA, -2 )
    grammar_to_analyse.vt.each{|s|
      $tmp_array << s
      re_sa.change_expression( JOptionPane.showInputDialog (nil,"Regular expression for '#{s}':  ","RE",JOptionPane::QUESTION_MESSAGE) )
      $la.addAnalizer AnalizerNFA.new(re_sa.create_NFA, token)
      token += 1
    }

    #here we are going to chance the analizer   
    if $grammarType == 1
      $grammarAnalizer = LL_Analizer.new(grammar_to_analyse)
    elsif $grammarType == 2
      $grammarAnalizer = SLR.new(grammar_to_analyse)
    elsif $grammarType == 3
      $grammarAnalizer = LR1_Analizer.new(grammar_to_analyse)
    elsif $grammarType == 4
      $grammarAnalizer = LR1_Analizer.new(grammar_to_analyse)
      $grammarAnalizer_result = $grammarAnalizer.result
      $grammarAnalizer_result.each{|x| sets.push($grammarAnalizer.new_Set(x))}
      simplify = $grammarAnalizer.simplify_sets(sets)
      $grammarAnalizer.new_table(simplify)
    end
    $expresion.setEnabled true
    $analyse.setEnabled true
  end
end


class Analize_grammar
  include java.awt.event.ActionListener
  def actionPerformed(evt)

    $la.change_string $expresion.getText
    
    #Lexically analyzes the input string
    string = Array.new
    while (token=$la.next_token)
      if token == -1
        puts "Lexically erroneous string"
        exit
      elsif token != -2  #Skips blank spaces       
        string << $tmp_array[token]
      end
    end
    string << "$"

    #just for LL(1) analysis 
    if $grammarType == 1
      result = $grammarAnalizer.analize(string)
    elsif $grammarType == 2
      result = $grammarAnalizer.analize(string)
    elsif $grammarType == 3 || $grammarType == 4
      result = $grammarAnalizer.verify_string(["0"],string)
    end     
      
    if result==false
      JOptionPane.showMessageDialog nil, "Syntacticly erroneous string","Error", JOptionPane::ERROR_MESSAGE
    elsif result == -1
      JOptionPane.showMessageDialog nil, "Abiguous Grammar ","Error", JOptionPane::ERROR_MESSAGE
    else
      output = "<!DOCTYPE html> <html <head>
      <title>Syntactical Analyzer</title>
      <link rel='stylesheet' href='table.css' type='text/css'/>
      </head> <body style='background:#A9D0F5'> <table> <tr>
      <th>Stack</th><th>String</th><th>Action</th>
      </tr>#{result}</table></body></html>"

      frame = JFrame.new
      panel = JPanel.new
      scroll = JScrollPane.new panel,JScrollPane::VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane::HORIZONTAL_SCROLLBAR_AS_NEEDED

      table = JEditorPane.new
      table.setEditable false
      table.setContentType("text/html")
      table.setText output

      panel.add table
      frame.add scroll
      frame.pack
      frame.setVisible true
      frame.setLocationRelativeTo nil
    end

  end
end

Menu.new
