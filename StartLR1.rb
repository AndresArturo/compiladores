require './Libraries/SyntacticAnalizer/LR1_Analizer'
require './Libraries/SyntacticAnalizer/RE_SyntacticAnalizer'

#Arguments must be of the form: grammar.txt "string to analyze"
#Make sure to scape correctly #{string to analyze} as there can
#be conflicts with the reserved symbols of the RE_Lexical_Analyzer 

#Reads the file containing the grammar
text_grammar = ""
IO.readlines(ARGV[0]).each{|line| text_grammar+=line}
grammar = GG_SyntacticAnalizer.new(text_grammar).analize

#print grammar.vt.to_a,"\n",grammar.vn.to_a,"\n"

#Creates the lexical analyzer asking for regular expressions
token = 0
tmp_array = Array.new
la = LexicalAnalizer.new "",""
grammar.vt.each{|s|
	tmp_array << s
	print "Regular expression for '#{s}':  "
	la.addAnalizer AnalizerNFA.new(RE_SyntacticAnalizer.new(STDIN.gets).create_NFA, token)
	token += 1
}

lr1_result = LR1_Analizer.new(grammar)


begin 
	print "\nString to analyse: "
	str = STDIN.gets.chomp()
	puts
	la.change_string str

	#Lexically analyzes the input string
	string = Array.new
	while (token=la.next_token)
		if token==-1
			puts "Lexically erroneous string"
			exit
		else
			string << tmp_array[token]
		end
	end
	string << "$"

	#LL(1) analysis 
	result = lr1_result.verify_string(["0"],string)

	if result==false
		puts "Syntacticly erroneous string"
	else
		out = File.new("lr1.html", "w")
		out.puts("<!DOCTYPE html>", "<html>", "<head>",
			"<title>LR1 syntactical analyzer</title>",
			"<link rel='stylesheet' href='table.css' type='text/css'/>",
			"</head>", "<body>", "<table>", "<tr>",
			"<th>Stack</th>","<th>String</th>","<th>Action</th>",
			"</tr>",result,"</table>","</body>","</html>")
		out.close
		puts "Accepted string, generated: lr1.html"
	end
	
	print "Continue? (y/n): "
	opc = STDIN.gets.chomp()
	
end while opc=="y"