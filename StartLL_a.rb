require './Libraries/SyntacticAnalizer/LL_Analizer'
require './Libraries/SyntacticAnalizer/RE_SyntacticAnalizer'

#Arguments must be of the form: "grammarFileName" "outputFileName" "string to analyze"
#Make sure to scape correctly #{string to analyze} as there can
#be conflicts with the reserved symbols of the RE_Lexical_Analyzer 


#Reads the file containing the grammar
text_grammar = ""
IO.readlines(ARGV[0]).each{|line| text_grammar+=line}
grammar = GG_SyntacticAnalizer.new(text_grammar).analize


#Creates the lexical analyzer asking for regular expressions
token = 0
tmp_array = Array.new
la = LexicalAnalizer.new "",ARGV[2]
grammar.vt.each{|s|
	tmp_array << s
	print "Regular expression for '#{s}':  "
	la.addAnalizer AnalizerNFA.new(RE_SyntacticAnalizer.new(STDIN.gets).create_NFA, token)
	token += 1
}

#Lexically analyzes the input string
string = Array.new
while (token=la.next_token)
	if token==-1
		puts "Lexically erroneous string"
		exit
	else
		string << tmp_array[token]
	end
end
string << "$"


#LL(1) analysis 
ll = LL_Analizer.new(grammar)
ll.print_table
ll_result = ll.analize(string)

if ll_result==false
	puts "Syntacticly erroneous string"
	exit
elsif ll_result==-1
	puts "Ambiguous grammar (multiple entries in a single cell in the LL table)"
	exit
else
	out = File.new(ARGV[1], "w")
	out.puts("<!DOCTYPE html>", "<html>", "<head>",
		"<title>LL syntactical analyzer</title>",
		"<link rel='stylesheet' href='table.css' type='text/css'/>",
		"</head>", "<body>", "<table>", "<tr>",
		"<th>Stack</th>","<th>String</th>","<th>Action</th>",
		"</tr>",ll_result,"</table>","</body>","</html>")
	out.close
	puts "Accepted string, generated: #{ARGV[1]}"
end
