class RE_Tokens
	UNION = 70			# OR
	CONCAT = 20  		# &
	P_CLOSURE = 10		# +
	K_CLOSURE = 30		# *
	ALTERNATIVE = 40	# ?
	B_LEFT = 50			# (
	B_RIGHT = 60		# )
	SYMBOL = 90			# whatever symbol, if used for other action, then escape it with | 
	RANGE = 80			# ...
	SPACES = 100		# Any number of continuous spaces
	SCAPE_CHAR = '!'	#! Not a token
end