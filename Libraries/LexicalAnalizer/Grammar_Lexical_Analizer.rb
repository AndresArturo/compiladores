require './Libraries/LexicalAnalizer/LexicalAnalizer'
require './Libraries/LexicalAnalizer/Grammar_Tokens'
require './Libraries/LexicalAnalizer/RE_Tokens'
require './Libraries/SyntacticAnalizer/RE_SyntacticAnalizer'

class Grammar_Lexical_Analizer < LexicalAnalizer

	def initialize(expression)
		nFAs = Array.new
		re = RE_SyntacticAnalizer.new("")

		#Rule
		re.change_expression "-&>"
		nFAs << re.create_NFA

		#Or (rule alternative)
		nFAs << NFA.new("|","")
		  
		#Delimiter
		nFAs << NFA.new(";","")
		  
		#Symbols, everything among the symbol delimiters
		# Detects only the first delimiter and then tricks during the next_token method
		nFAs << NFA.new("#{Grammar_Tokens::SYMBOL_DELIMITER}","")

		#accepting (ignoring) blank spaces, break lines, tabs, etc.
		re.change_expression "(#{RE_Tokens::SCAPE_CHAR}  OR \n OR \t)+"
		nFAs << re.create_NFA

        #creation of the lexical analizer
        super "Grammar lexical analizer",expression
		  
		token = 10;
		nFAs.each do |nfa|
			addAnalizer( AnalizerNFA.new nfa,token )
			token += 10
		end 
	end
	

	# Wraps and adapts the next_token method of the super class.
	# The erroneous state now ocurres only when the symbol delimiters
	# are not closed well or doesn't contain anything.
	def next_token
		tmp = super

		if tmp == Grammar_Tokens::BLANK_SPACE #Ignores the blank spaces
			tmp = next_token
		elsif tmp == Grammar_Tokens::SYMBOL 
			begin
				if @strIndex == @string.length
					tmp = Grammar_Tokens::ERROR
					break
				end
				@current_lexeme += @string[@strIndex]
				@strIndex += 1
			end while @string[@strIndex-1] != Grammar_Tokens::SYMBOL_DELIMITER
		end
		
		return tmp
	end


	#Wraps and adapts the method to avoid returning the symbol delimiters
	def current_lexeme
		return (@current_lexeme[0]==Grammar_Tokens::SYMBOL_DELIMITER ? @current_lexeme[1...(@current_lexeme.length-1)] : @current_lexeme)
	end
	

	#Object copy 
	def deep_clone
    	return Marshal::load(Marshal::dump(self))
  	end

end 