require "./Libraries/LexicalAnalizer/AnalizerNFA"

class LexicalAnalizer
	
	attr_reader :name
	attr_accessor :string
	attr_reader :current_lexeme #The last found lexeme during the last analisis


	def initialize(name, string)
		@name = name
		@string = string
		@strIndex = 0
		@current_lexeme = ""
		@nfas = Array.new
	end


	#Add all the individual AnalizerNFA's
	#!The token shouldn't be -1
	def	addAnalizer(*analizerNFA)
		@nfas += analizerNFA
	end


	#Use this method if the target string changes or without
	#attributes if it's desired to restart the analisis
	def change_string(string=@string)
		@string = string
		@strIndex = 0
		@current_lexeme = ""
	end
	
	
	#Object copy 
	def deep_clone
    	# Abracadabra !!!
    	return Marshal::load(Marshal::dump(self))
  	end
	

	#Analizes the rest of the string seeking for the next match.
	#Uses all the given automatons, moves the pointer to the string's position
	#and finally returns the token of the next identified lexema or NIL if the
	#string has been finished. On error returns -1.
	#Be careful with automatons recognizing empty strings, if they detect a symbol
	#which they can't accept, the return will be a positive result and the lexeme
	#will be an empty string, the pointer will also advance anyways.
	def next_token
		maxSymbolsFound = 0
		
		while @strIndex < @string.length do
			maxSymbolsFound = -1
			longestAnalizerIndex = -1
		
		    nfasIndex = 0
			
			while nfasIndex < @nfas.count do
				aux = @nfas.at(nfasIndex).analizeSimply(@string[@strIndex..@string.length])
				
				if aux > maxSymbolsFound
					maxSymbolsFound = aux
					longestAnalizerIndex = nfasIndex
				end 

				nfasIndex += 1
			end 
			
			aux = @strIndex
			@strIndex += maxSymbolsFound < 1 ? 1 : maxSymbolsFound

			if maxSymbolsFound != -1
				@current_lexeme = @string[aux...(aux+maxSymbolsFound)]
				return @nfas.at(longestAnalizerIndex).token
			elsif maxSymbolsFound < 1
				@current_lexeme = @string[aux]
				return -1
			end
		end
	end


	#Rewinds the string so that the next call to next_token sets the analizer to the same state
	#The current_lexeme is set to empty.
	#! This method should just be used once between calls to next_token
	def rewind
		@strIndex -= @current_lexeme.length
		@current_lexeme = "" #Prevents this method to cause any effects due to multiple calls
	end 
end