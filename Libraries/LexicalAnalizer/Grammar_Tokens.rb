class Grammar_Tokens
	RULE = 10			# ->
	OR = 20  			# |
	DELIMITER = 30		# ;
	SYMBOL = 40			# whichever string among the SYMBOL_DELIMITER
	BLANK_SPACE = 50	# any kind of blank space (space, line break, tab, etc)
	SYMBOL_DELIMITER = "'" #! Not a token
	ERROR = -1 			# the string can't be recognized by this analizer
end