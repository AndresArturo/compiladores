require './Libraries/LexicalAnalizer/LexicalAnalizer'
require './Libraries/LexicalAnalizer/RE_Tokens'

class RE_Lexical_Analizer < LexicalAnalizer

	def initialize(expression)
		nFAs = Array.new

		#individual symbols
		nFAs << NFA.new("+","")
		nFAs << NFA.new("&","")
		nFAs << NFA.new("*","")
		nFAs << NFA.new("?","")
		nFAs << NFA.new("(","")
		nFAs << NFA.new(")","")
		  
		#union (OR) 
		aux = NFA.new("O","")
		aux.concatenate NFA.new("R","")
		nFAs << aux

		#range (...) 
		aux = NFA.new(".","")
		aux.concatenate NFA.new(".","")
		aux.concatenate NFA.new(".","")
		nFAs << aux
		  
		#Symbols
		#Due to performance matters, the detection of symbols will be carried
		#out by non-detecting other non-symbols (i.e an otherwise erroneous state).
		#The only symbol which will be detected here is the escape character '|'
		#so that it can be tricked in the here specialized next_token method.
		aux = NFA.new(RE_Tokens::SCAPE_CHAR,"")
		nFAs << aux

		#accepting (ignoring) blank spaces
		aux = NFA.new(" ","")
		aux.positive_closure
		nFAs << aux

        #creation of lexical analyzer
        super "NFA constructor",expression
		  
		token = 10;
		nFAs.each do |nfa|
			addAnalizer( AnalizerNFA.new nfa,token )
			token += 10
		end 
	end
	

	# Wraps and adapts the next_token method of the super class.
	# The erroneous state now happens only if there is an escaping character
	# at the end of the string.
	def next_token
		tmp = super

		if tmp == -1 #Nothing was detected, therefore a symbol is considered
			tmp = RE_Tokens::SYMBOL
		elsif tmp == RE_Tokens::SYMBOL #An escape character was detected
			if @strIndex == @string.length #It was the last character
				tmp = -1 #Erroneous recognition
			else
				@current_lexeme = "#{RE_Tokens::SCAPE_CHAR}#{@string[@strIndex]}" #Makes the next char the current symbol
				@strIndex += 1
			end
		elsif tmp == RE_Tokens::SPACES #Ignores the spaces
			tmp = next_token #Calls itself again
		end
		#puts tmp
		return tmp
	end
	
	
	#Object copy 
	def deep_clone
    	return Marshal::load(Marshal::dump(self))
  	end


	#Wraps and adapts the method to prevent returning the scape character
	def current_lexeme
		return @current_lexeme[0]==RE_Tokens::SCAPE_CHAR ? @current_lexeme[1...@current_lexeme.length] : @current_lexeme
	end

end 