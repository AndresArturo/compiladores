require './Libraries/NFA/NFA'

class AnalizerNFA
	
	attr_reader :token
	

	def initialize(nfa, token)
		@nfa = nfa
		@token = token
	end
	

	#Analizes a string in a single pass returning the number of detected symbols,
	#-1 if non or 0 if it accepted just an empty string E
	def analizeSimply(string)
		currentState = @nfa.e_c
		lastAcceptedSymbolIndex = (@nfa.acceptance_state? currentState) ? 0 : -1 

		for symbolIdx in (0...string.length) #For each symbol
			currentState = @nfa.go_to string[symbolIdx], currentState #Transition is tested for the current symbol
			if !currentState.empty?
				if @nfa.acceptance_state? currentState #An acceptance state reached
					lastAcceptedSymbolIndex = symbolIdx + 1
				end	
			else #When it finds the first non-recognized symbol
				break
			end
		end

		return lastAcceptedSymbolIndex
	end

end