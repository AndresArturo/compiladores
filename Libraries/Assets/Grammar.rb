require 'set'

class Grammar

	attr_reader :vn
	attr_reader :vt
	attr_reader :allLeftSymbols
	attr_reader :rulesList
	EPSILON = ""
	

	def initialize(rulesList)
		@rulesList = rulesList	#Hash type
		@allLeftSymbols = Set.new 
		@vn = get_NT_symbols	#Set type
		@vt = get_T_symbols     #Set type
		
	end


	def getInitial
		rulesList.each_key{|nts| return nts}
	end
	

	#returns true if it's not terminal symbol otherwise false
	def isNotTerminal?(symbol) 
		return @vn.include?(symbol)
	end
	

	#returns true if it's terminal symbol otherwise false
	def isTerminal?(symbol)
		return @vt.include?(symbol)
	end
	

	#Returns a new array containing all the productions of the given symbol
	def getProductions(symbol)
		@rulesList.each{|key,value|
			if(key== symbol)
				return value + [] #Creates a new array
			end
		}
	end


	def rule_number(left, right)
		count = 0
		@rulesList.each{|key,value|
			if(key == left)
				value.each { |str|
					if str.eql? right
						return count
					end
					count += 1
				}
			end
			count += value.size
		}
	end


	# Returns [left, right]
	# right = ["E","F"]
	def rule(number)
		@rulesList.each{|key,value|
			value.each { |str|
				if(number == 0)
					return [key, str|[]]
				end
				number -= 1
			}			
		}
	end


	def augment
		newHash = Hash.new
		i = 0
		
		@rulesList.each{|key,value|
			if i== 0 			
				auxArray = Array.new
				leftSide = Array.new
				leftSide.push(key)
				auxArray.push(leftSide)	
				
				newHash["#{key}p"] = auxArray
			end
			
			newHash[key] = value
			i=i+1
		}

		@rulesList = newHash
		@allLeftSymbols = Set.new 
		@vn = get_NT_symbols
		@vt = get_T_symbols
	end

	
	private 
	
	#Returns a Set which contains all the nonTerminal Symbols
	def get_NT_symbols
		nonTerminalSymbols = Set.new
		@rulesList.each{|leftSymbol,rules| 
				nonTerminalSymbols.add(leftSymbol)
		}
		return nonTerminalSymbols
	end

	#Returns a Set which contains all the Terminal Symbols
	def get_T_symbols
		leftSymbols = Set.new
		allSymbols  = Set.new 
		@rulesList.each{|lSymbols,rules| 
			leftSymbols.add(lSymbols)
			rules.each{|rule| 
				rule.each{|symbol| allSymbols.add(symbol)}
			}
		}
		leftSymbols.add(EPSILON)
		terminalSymbols = allSymbols - leftSymbols	
		leftSymbols.each{|i|
			if i!=""
				@allLeftSymbols.add(i)
			end
		}
		return terminalSymbols
	end
end