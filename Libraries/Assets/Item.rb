require './Libraries/Assets/Grammar'

class Item

	attr_reader :point
	attr_reader :left
	attr_reader :right

	# left := a symbol representing the left part of the rule
	# right := ["simb1","simb2",...]
	# point := point index. If 1, the item represents ["simb1", point, "simb2", ...]
	def initialize(left, right, point)
		@left = left
		@right = right + [] #creates a new array
		@point = point
	end


	# Returns an array containing all the possible items for a rule
	# Can be called without and instance of the class
	def self.items(left, right)
		items = Array.new

		(0..right.size).each{|i| #takes in account the last possibility
			items.add new Item(left, right, i)
		}

		return items
	end


	# Redefines the comparison among Items
	def == (other)
	    @point == other.point && @left == other.left && @right.eql?(other.right)
	end


	def eql?(other)
		self == other
	end


	def hash
		[@point, @left, @right.hash].hash
	end


	# Returns the symbol next to the point, EPSILON if the point is in the last possition
	def nextSymbol
		@right[@point]==nil ? Grammar::EPSILON : @right[@point]
	end


	# Returns the symbol previous to the point or EPSILON if the point is in the first possition
	def prevSymbol
		@point==0 ? Grammar::EPSILON : @right[@point-1]
	end


	# Returns a new item with the point advanced one place. Nil if not possible.
	def nextItem
		@point==@right.size ? nil : Item.new( @left, @right, @point + 1)
	end


	# Returns a new item with the point rewinded one place. Nil if not possible.
	def prevItem
		@point==0 ? nil : Item.new( @left, @right, @point - 1)
	end


	def printa
		print left + " -> (#{point})   "
		print right
		puts ""
	end


end