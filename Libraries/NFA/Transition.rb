require './Libraries/NFA/State'

class Transition

	def initialize(symbol="", followingState=State.new)
		@symbol = symbol
		@followingState = followingState
	end

	#Evaluates if the next state can be reached with the given symbol and, if so,
	#returns it or nil otherwise.
	def move(symbol="")
		return @symbol == symbol ? @followingState : nil
	end

end