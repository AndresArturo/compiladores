require './Libraries/NFA/Transition'

class State

	@@statesCounter = 1

	attr_reader :accept #An status variable indicating wether this is an acceptance state or not
	attr_reader :id #A global identifier of this state
	#attr_reader :transitions #The set of available transitions from this state

	def initialize(accept = false)
		@id = @@statesCounter
		@@statesCounter += 1
		@accept = accept
		@transitions = Array.new
	end
	
	
	def make_non_accepting
		@accept = false
	end
	

	#Adds a new transition to the current state.
	#By default, the new transition will add a new accepting state with an E transitioning symbol
	def add_transition(symbol="", state=State.new(true))
		@transitions << Transition.new(symbol, state)
	end


	#A move operation which works only for this state with the given symbol
	def move(symbol)
		states = Array.new

		@transitions.each do |transition|
			states << transition.move(symbol)
		end

		states.delete(nil)
		return states
	end

end