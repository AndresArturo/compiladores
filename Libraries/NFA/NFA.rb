require './Libraries/NFA/State'

class NFA


	#Creates a basic automaton with an initial and an acceptance state with the given transitioning symbol
	def initialize(symbol="", name)
		@name = name
		@initial = State.new
		@initial.add_transition(symbol)
		@acceptance = @initial.move(symbol)
	end

	attr_reader :initial #The initial state of the NFA 
	attr_accessor :name #An identifier of the automaton
	attr_reader :acceptance #The set of all the acceptance states to improve efficiency


	#An standard move operation
	#Thus always returns a set
	def move(symbol, states=@initial)
		result = Array.new
		if states.respond_to?("each")
			states.each do |state|
        		result |= state.move(symbol)
      		end
    	else
     		result = states.move(symbol)
    	end

    	return result
	end


	#An standar epsilon closure operation
	#Thus always returns a set
	def e_c(states=@initial)
		if !states.respond_to?("each") then states = [states] end

		newStates = states
		begin
   			newStates = move("", newStates)
   			states |= newStates
		end until newStates.empty?
		
		return states
	end


	#An standard go to operation
	def go_to(symbol, states=@initial)
		return e_c move(symbol, states) 
	end


	#Returns whether the given state is of acceptance or if an acceptance one can be reached from it
	#This method will return true if a set of states is given and the condition applies to at least one of them
	def acceptance_state?(state=@initial)
		e_c(state).each do |st|
			if st.accept then return true end
		end
		return false
	end


	#Joins the actual AFN with the given one and the result is stored in this one
	def union(nfa) 
		newInit = State.new
		newAccept = State.new true
		
		newInit.add_transition("", @initial)
		newInit.add_transition("", nfa.initial)
		@initial = newInit
		
		redirect_acceptance newAccept, nfa.acceptance | @acceptance
		@acceptance = [newAccept]
	end
	
	
	# Concatenates the actual AFN with the given one.
	# Makes a Concatenate operation among the actual and given AFN. While the actual
	# one remains with the result of the operation, the given one is made
	# obsolete, thus all references must be forgotten.
	def concatenate(nfa) 	
		redirect_acceptance nfa.initial
		@acceptance = nfa.acceptance
	end
	
	
	def positive_closure
		newInit = State.new
		newAccept = State.new true
		
		@acceptance.each do |accept| 
			accept.add_transition("", @initial)
			accept.make_non_accepting
			accept.add_transition("", newAccept)
		end

		newInit.add_transition("", @initial)
		@initial = newInit
		@acceptance = [newAccept]
	end
	
	
	def kleen_closure
		positive_closure

		@acceptance.each do |accept| 
			@initial.add_transition("", accept)
		end
	end
	
	
	def alternative
		newInit = State.new
		newAccept = State.new true
		
		newInit.add_transition("", @initial)
		@initial = newInit

		redirect_acceptance newAccept
		@initial.add_transition("", newAccept);
		@acceptance = [newAccept]
	end


	#Creates a transition from all the given accepting states to the given state
	def redirect_acceptance(state, acceptance_states=@acceptance)
		acceptance_states.each do |accept| 
			accept.make_non_accepting
			accept.add_transition("", state)
		end
	end
	
	
	#Object copy 
	def deep_clone
    	# Abracadabra !!!
    	return Marshal::load(Marshal::dump(self))
  	end


	private :redirect_acceptance


end