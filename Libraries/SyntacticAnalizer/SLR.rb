require './Libraries/Assets/Item'
require './Libraries/Assets/Grammar'
require 'set'

class SLR

	attr_reader :grammar
	#attr_reader :lr_table
	EOS = "$"

	def initialize(grammar)
		@grammar = grammar
		@grammar.augment
		@list = Array.new
		@S = getSets
		@lr_table = Array.new
		(0...@S.size).each { 
			array = Array.new
			(0...@grammar.vn.size + @grammar.vt.size).each {
				array << []
			}
			@lr_table << array
		}
		fill_table
	end

	
	#str = ['simb1','simb2','simb3',EOS]
	#Returns false if erroneous string, -1 if ambiguous grammar or a string representing the contents of an html table
	def analize(str, stack=["0"])
		out = "<tr><td>"

		if stack.size == 0 || str.size == 0
			return false
		else
			popped = stack[-1]
			action = getAction popped.to_i, str[0]

			stack.each{|e| out += "#{e} "} #Prints stack
			out += "</td><td>"
			str.each{|e| out += "#{e} "} #Prints string
			out += "</td><td>"

			if action.size == 0
				return false
			elsif action.size > 1
				return -1
			else
				action = action[0]

				if  action[0] == "d"
					out += action + "</td></tr>" # Prints action
					stack.push str[0]
					stack.push action[1...action.size]
					str = str[1...str.size]
				elsif action == "r0"
					return out + "r0: Accept</td></tr>"
				else  #Reduction
					rule = @grammar.rule(action[1...action.size].to_i)
					out += action + ": #{rule[0]} -> " # Prints action
					rule[1].each { |e| out += e }
					out += "</td></tr>"
					stack = stack[0...stack.size-rule[1].size*2]
					stack.push rule[0]
					action = getAction(stack[-2].to_i, stack[-1])
					if action.size == 0
						return false
					elsif action.size > 1
						return -1
					end
					stack.push action[0]
				end
				
				tmp = analize(str, stack)
				return (tmp==false ? false : (tmp==-1 ? -1 : out+tmp))
			end
		end
	end


	def print_table
		print "\t"
		@grammar.vt.each { |e| print "#{e}\t" }
		@grammar.vn.each { |e| print (e==@grammar.getInitial ? "$\t" : "#{e}\t") }
		puts ""

		row_num = 0
		@lr_table.each{ |row|
			print "#{row_num}\t"
			row.each{ |cell|
				cell.each { |entry|
					print "#{entry} "
				}
				print "\t"
			}
			puts ""
			row_num += 1
		}
	end



	private

	def fill_table
		i = 0

		@S.each { |si|  
			@grammar.vt.each { |x| 
				sj = go_to(si,x)
				if @S.include?(sj)
					set_table(i,x, "d#{@S.index(sj)}" )
				end
			}
			i += 1
		}

		i = 0
		@S.each { |si|  
			@grammar.vn.each { |x| 
				sj = go_to(si,x)
				if @S.include?(sj)
					set_table(i, x, "#{@S.index(sj)}" )
				end
			}
			i += 1
		}

		i = 0
		@S.each { |si|  
			si.each { |item| 
				if item.nextSymbol == Grammar::EPSILON
					follow(item.left).each { |symb| 
						set_table(i, symb, "r#{@grammar.rule_number(item.left, item.right)}")
					}
					clear_list
				end
			}
			i += 1
		}
	end


	# v E Vt u Vn 
	def set_table(i, v, value)
		@lr_table[i][x_index(v)] << value
	end


	# v E Vt u Vn 
	# Returns ["d5","r2"]
	def getAction(i, v)
		return @lr_table[i][x_index(v)]
	end


	def first(sigma) #sigma is an Array e.g ["Ep","Tp"]
		result = Set.new
		if sigma.size==0
			result.add("")
		elsif sigma[0]==""
			sigma.delete("")
			result.merge first(sigma)
		elsif @grammar.isTerminal? sigma[0]
			result.add(sigma[0])
		elsif @grammar.isNotTerminal? sigma[0]
			alphas = @grammar.getProductions(sigma[0])	
			alphas.each{|alpha|
				newAlpha = alpha.dup
				if sigma[1] != nil
				    newAlpha.push(sigma[1])
				else
					newAlpha.push("")
				end
				result.merge first(newAlpha)
				
			}
		end
		return result
	end 
	

	def clear_list
		@list = Array.new
	end


	def follow(rule)
		total = Set.new
		if(@list.include?(rule) == false)
			@list << rule
		end 
		result = Hash.new
		left = @grammar.rulesList.keys
		if(left[0].eql? rule)
			total << EOS
		end 
		
		@grammar.rulesList.each{|key,value|  #Rules witch contain "rule" on the right side
			value.each{|rules| 
				if(rules.include? rule)
					aux = Array.new
					aux << rules
					if(result.has_key?(key))
						auxArray = result[key]
						auxArray.each{|symb| aux << symb}
					end
					result[key] = aux
				end
			}
		}
		
		result.each{|key,value|  
			value.each{|rules| 
				if(rules.include? rule)
					if((rules.size)-1 == rules.index(rule))
						if(@list.include?(key) == false)
							total = total | follow(key)
						end
					end
					
					if(rules.index(rule) < (rules.size)-1)
						aux = first(rules.drop(rules.index(rule)+1))
						aux = aux.to_a
						if(aux.include? "")
							aux.delete_at(aux.index(""))
							total = total | follow(key)
						end
						aux = aux.to_set
						total = total | aux
						
					end
				end
			}
		}
		return total
	end


	# v E Vt u Vn 
	def x_index(v)
		x = 0

		(@grammar.vt|@grammar.vn).each { |e|  
			if e==v || v==EOS && e==@grammar.getInitial
				return x
			else
				x += 1
			end
		}
	end


	# Can return an empty set
	def move(aSet, alpha)
		bSet = Set.new

		aSet.each { |item| 
			if item.nextSymbol == alpha
				bSet << item.nextItem
			end
		}

		return bSet
	end


	def c_E(aSet)
		aSet = aSet.to_a

		aSet.each { |item|
			if @grammar.vn.include? item.nextSymbol
				@grammar.getProductions(item.nextSymbol).each{ |str|
					newItem = Item.new(item.nextSymbol,str,0)
					if !aSet.include? newItem
						aSet << newItem
					end
				}
			end
		}

		Set.new aSet
	end


	def go_to(aSet, alpha)
		return c_E move(aSet, alpha)
	end


	def getSets
		s = [c_E( Set.new([Item.new(@grammar.getInitial, @grammar.getProductions(@grammar.getInitial)[0], 0)]) )] # S0

		s.each { |si|
			s_poss(si).each { |aux|
				if !s.include?(aux)
					s << aux
				end
			}  # Recalculates for every new Si
		}

		s.delete Set.new # Excludes the empty set

		return s
	end


	# Given a set of items, gets all the possible Si derived from it
	def s_poss(iSet)
		s = Array.new

		iSet.each { |item|
			if item.nextSymbol != Grammar::EPSILON
				s << go_to(iSet, item.nextSymbol)
			end
		}

		return s
	end

end