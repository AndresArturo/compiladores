require './Libraries/Assets/Grammar'
require './Libraries/Assets/Thing'
require 'set'

class LR1_Analizer
	attr_reader :grammar
	attr_reader :list
	attr_reader :result
	attr_reader :table
	
	def initialize(grammar)
		@list = Array.new
		@result = Array.new 
		@table = Hash.new
		@grammar = grammar #@grammar.rulesList if you wanna work without dots
		@hashRulesList = aumentedGrammar(Marshal.load(Marshal.dump(grammar.rulesList))) #works with dots, it is a hash
		@arrayRulesList = hashToArray(@hashRulesList)  #Array of Arrays: the position of the array describe the rule number
	
		firstRule = Marshal.load(Marshal.dump(@arrayRulesList[0]))
		firstRule.push("$")
		auxArray = Array.new 
		auxArray.push(firstRule)
		
		s0 = closure(auxArray)
		@result = analysis(s0)
		
		@table.each{|k,v|
			print "#{k},#{v} \n"
		}
		print "********************************\n"
		puts
	end
	
	def new_Set(set)
		newSet = Array.new
		newSymbol = Array.new
		result = Array.new
		set.each{|x|
				aux = Array.new
				aux1 = Array.new
				aux.push(x[0])
				aux.push(x[1])
				aux1.push(x[2])
				if !newSet.include?(aux)
					newSet.push(aux)
					newSymbol.push(aux1)
				else
					indice = newSet.index(aux)
					newSymbol[indice].push(x[2])
				end
		}
		result.push(newSet)
		result.push(newSymbol)
		return result
	end
	
	def simplify_sets(list_set)
		newSet = Array.new
		newSymbol = Array.new
		number = Array.new
		result = Array.new
		list_set.each_with_index do |x, index|
					if !newSet.include?(x[0])
						newSet.push(x[0])
						newSymbol.push(x[1])	
					else
						indice = newSet.index(x[0])
						for i in 0..newSymbol[indice].size-1
							x[1][i].each{|y|
									newSymbol[indice][i].push(y)}
						end
					end
		end
		newSet.each{|x|
			number_aux = Array.new
			list_set.each_with_index do |y, index|
				if x == y[0]
					number_aux.push(index.to_s())
				end
			end
			number.push(number_aux)
		}
		for i in 0..newSet.size-1
			aux = Array.new
			aux.push(number[i])
			aux.push(newSet[i])
			aux.push(newSymbol[i])
			result.push(aux)
		end
		return result
	end

	def new_table(list_set)
		newTable = Hash.new
		combine = Array.new
		verify = Array.new
		
		list_set.each{|x|
				if x[0].size>1
					combine.push(x[0])
					x[0].each{|y|
						verify.push(y)}
				end
		}
		if combine.size>0
			list_set.each{|x|
					if x[0].size==1
						@table.each{|y|
								if y[1][0].include?("d")
									if y[0][0] == x[0][0]
										combine.each{|z|
											z.each{|i|
												if y[1][1]==i
													aux = Array.new
													aux.push(y[1][0])
													aux.push(rename(z))
													newTable[y[0]] = aux
												else
													if !newTable.has_key?(y[0])
														newTable[y[0]] = y[1]
													end
												end
											}
										}	
									end
								elsif !verify.include?(y[0][0]) and y[1][0].include?("r")
									newTable[y[0]] = y[1]
								elsif !verify.include?(y[0][0])
									if y[0][0] == x[0][0]
										combine.each{|z|
											z.each{|i|
												if y[1][0]==i
													aux = Array.new
													aux.push(rename(z))
													newTable[y[0]] = aux
												else
													if !newTable.has_key?(y[0])
														newTable[y[0]] = y[1]
													end
												end
											}
										}	
									end
								end
						}
					else
						@table.each{|y|
								if y[1][0].include?("d")
									if y[0][0] == x[0][0]
										combine.each{|z|
											z.each{|i|
												if y[1][1]==i
													aux = Array.new
													key = Array.new
													aux.push(y[1][0])
													aux.push(rename(z))
													key.push(rename(x[0]))
													key.push(y[0][1])
													newTable[key] = aux
												end
											}
										}	
									end
								elsif y[1][0].include?("r")
									x[0].each{|i|
										if y[0][0] == i
											key = Array.new
											key.push(rename(x[0]))
											key.push(y[0][1])
											newTable[key] = y[1]	
										end
									}
								else
									x[0].each{|i|
										if y[0][0] == i.to_s()
											combine.each{|z|
												z.each{|j|
													if y[1][0]==j
														aux = Array.new
														key = Array.new
														aux.push(rename(z))
														key.push(rename(x[0]))
														key.push(y[0][1])
														newTable[key] = aux
													end
												}
											}	
										end
									}
								end
						}	
					end
			}
			@table = newTable
			newTable.each{|k,v|
				print "#{k},#{v} \n"}
			print "********************************\n"
		end
	end
	
	def rename(array)
		out = ""
		array.each_with_index do |x, index|
				out += x.to_s()
				if index!=array.size-1
					out += "-"
				end
		end
		return out
	end
	
	def verify_string(stack,str)
		action = @table[[stack.last,str[0]]]
		
		if action != nil
			out = "<tr><td>"
			stack.each{|e| out += "#{e} "} #Prints stack
			out += "</td><td>"
			str.each{|e| out += "#{e} "} #Prints string
			out += "</td><td>"
			action.each{|e| out += "#{e}"} #Prints action
		else
			return false
		end
			
		if action!=nil && action == ["r","0"]
			out += "</td></tr><td>"
			return out += "</td><td></td><td>Accept</td></tr>"			
		elsif action!=nil && action.include?("r")  #it is a reduction
			rule = @arrayRulesList[action[1].to_i]
			size = (rule[1].size-1)*2
			
			leftSide = Marshal.load(Marshal.dump(rule[0]))
			rightSide = Marshal.load(Marshal.dump(rule[1]))
			rightSide.delete(".")
			out += " ,  "
			out += leftSide
			out += " -> "
			rightSide.each{|e| out += "#{e} "} #Prints rule
			out += "</td></tr>"
		
			for i in 1..size
				stack.pop
			end
					
			stack.push(rule[0])
			actionAux = @table[[stack[stack.size-2],stack.last]]
			stack.push(actionAux[0])
			
			tmp = verify_string(stack,str)
			if tmp != false
				return out += tmp
			else
				return false
			end			
		elsif action!=nil && action.include?("d")  #it is a movement
			out += "</td></tr>"
			
			stack.push(str[0])
			stack.push(action[1])
			str.delete_at(0)
			
			tmp = verify_string(stack,str)
			if tmp != false
				return out += tmp
			else
				return false
			end
		else 						#the rule was not found
			return false
		end 
	end
	
	def analysis(auxS0)
		s0 = Marshal.load(Marshal.dump(auxS0))
		setOfStates = Array.new
		setOfStates[0] = s0
		i = 1
		j=0
				
		setOfStates.each{|state|
			setOfPreAnalysis = getSetOfPreAnalysis(state)
			setOfPreAnalysis.each{|symbol|						
				newState = go_to(state,symbol)	
				if !setOfStates.include?(newState)
					setOfStates[i] = newState	
					i=i+1
				end
				
				key=Array.new
				key=[setOfStates.index(state).to_s,symbol]
				
				value=Array.new
				if @grammar.isTerminal? symbol
					value = ["d",setOfStates.index(newState).to_s]
				else 
					value = [setOfStates.index(newState).to_s]
				end 				
				@table[key] = value
				
				if getSetOfReductions(newState).size != 0 #there is productions
					symbols = getSetOfReductions(newState)[1]
					symbols.each{|symbol|
						key = Array.new 
						key = [setOfStates.index(newState).to_s,symbol]
						
						value = Array.new
						value = ["r",@arrayRulesList.index(getSetOfReductions(newState)[0]).to_s]
						@table[key] = value
					}
				end
			}
			j+=1
		}	
		return setOfStates
	end 
	
	
	def go_to(set,symbol)
		return closure(move(set,symbol))
	end 
	
					  #{ [Ep->.E,$], ... }
	def closure(set)  #[ [ "Ep",["E", "."],"$"] , ... ],set:Array of Arrays
		arrayResult = Marshal.load(Marshal.dump(set))
	
		arrayResult.each{|rule|
			resultAnalysis = analysRule(rule)
	
			resultAnalysis.each{|auxRule|
				if !arrayResult.include?(auxRule)
					arrayResult.push(auxRule)
				end
			}
		}
		return arrayResult
	end 

	
	def move(set,symbol) #set: Array of arrays => [ ["Ep", [".", "E"],["$","+"]], ["E", [".", "E", "+", "T"],["$","+"]] ], symbol:String => "E"

		itemsSet = Marshal.load(Marshal.dump(set))
		result = Array.new
		
		itemsSet.each{|rule|
			leftSide = rule[0]
			rightSide = Marshal.load(Marshal.dump(rule[1]))
			dotPosition = rightSide.index(".")
			
			if rightSide[dotPosition+1] == symbol
				rightSide.delete(".")
				rightSide.insert(dotPosition+1,".")
				rule[1] = rightSide
				result.push(rule)
			end
		}
		return result
	end
	
	
	def first(sigma) #sigma is an Array e.g ["Ep","Tp"]
		result = Set.new
		if sigma.size==0
			result.add("")
		elsif sigma[0]==""
			sigma.delete("")
			result.merge first(sigma)
		elsif @grammar.isTerminal?(sigma[0]) || sigma[0]=="$"
			result.add(sigma[0])
		elsif @grammar.isNotTerminal? sigma[0]
			
			alphas = @grammar.getProductions(sigma[0])	
			alphas.each{|alpha|				
				newAlpha = alpha.dup
				if sigma[1] != nil
				    newAlpha.push(sigma[1])
				else
					newAlpha.push("")
				end
				result.merge first(newAlpha)
			}
		end
		return result
	end 
	
	
	def aumentedGrammar(hash)
		newHash = Hash.new
		i=0
		
		hash.each{|key,value|
			if i== 0 			
				auxArray = Array.new
				leftSide = Array.new
				leftSide.push(key)
				auxArray.push(leftSide)	
				
				newHash["#{key}p"] = auxArray
			end
			newHash[key] = value
			i=i+1
		}
		return newHash
	end
	
	
	def hashToArray(rulesList)
		newRulesList = Array.new
		i=0
		rulesList.each{|key,value| 
				value.each{|rules| 
					aux = Array.new 
					aux.push(key)
					aux.push(rules.insert(0,"."))
					newRulesList[i] = aux
					i=i+1
				} 
		}
		return newRulesList
	end 
	
	
	def getSetOfPreAnalysis(set) #return all the symbols which has previously a dot, set: Array => [ ["Ep", [".", "E"],"$"], ... ]
		result = Set.new 
		set.each{|rule|
			rightSide = rule[1]
			if rightSide.index(".") != (rightSide.size-1)
				result.add(rightSide[rightSide.index(".")+1])
			end
		}
		return result.to_a 
	end
	
	
	def analysRule(auxRule) # rule:Array ["Ep", [".", "E"],"$"]
		rule = Marshal.load(Marshal.dump(auxRule))
		result = Set.new     ## puede eliminarse
		leftSide = rule[0]
		rightSide = rule[1]
		preSymbol = rule[2]
		result.add(rule)

		if (rightSide.size-1) != rightSide.index(".") #the rule there is no have more analysis
			if	grammar.isNotTerminal?(rightSide[rightSide.index(".")+1]) 
				productions = @hashRulesList[rightSide[rightSide.index(".")+1]]
								
				if (rightSide.size-rightSide.index(".")) == 2 #the next symbol is epsilon
						sigma = Array.new 
						sigma.push("")
						sigma.push(preSymbol)
											
						resultFirst =  first(sigma)
		
						resultFirst.each{|element|
							productions.each{|production|
								newRule = Array.new
								newRule.push(rightSide[rightSide.index(".")+1])
								newRule.push(production)
								newRule.push(element)
								result.add(newRule)
							}
						}
				else
						sigma = Array.new 
						sigma.push(rightSide[rightSide.index(".")+2])
						sigma.push(preSymbol)
					
						resultFirst =  first(sigma)

						resultFirst.each{|element|
							productions.each{|production|
								newRule = Array.new
								newRule.push(rightSide[rightSide.index(".")+1])
								newRule.push(production)
								newRule.push(element)
								result.add(newRule)
							}
						}
				end 
			end
		end
		return result
	end

	def getSetOfReductions(auxState) 
		state = Marshal.load(Marshal.dump(auxState))
		set = Array.new 
		
		state.each{|rule|
			rightSide = rule[1]
			if rightSide.index(".") == (rightSide.size-1)
				set.push(Marshal.load(Marshal.dump(rule)))
			end
		}

		newRule = Array.new
		if set.size != 0
			rule = set[0]
			leftSide = Marshal.load(Marshal.dump(rule[0]))
			rightSide = Marshal.load(Marshal.dump(rule[1]))
			rightSide.delete(".")
			rightSide.insert(0,".")
			
			newRule=[[leftSide,rightSide]]
			
			symbols = Array.new
			set.each{|auxRule| 
				symbols.push(auxRule[2])
			}
			newRule.push(symbols)
		end 
		return newRule
	end 	
end 