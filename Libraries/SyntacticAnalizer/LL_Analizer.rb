require './Libraries/Assets/Grammar'
require 'set'


class LL_Analizer
	attr_reader :grammar
	#attr_reader :list
	#attr_reader :LL_list
	

	def initialize(grammar)
		@grammar = grammar
		@list = Array.new
		@LL_list = get_LL_table
	end
	
	
	#str = ['simb1','simb2','simb3']
	#Returns false if erroneous string, -1 if ambiguous grammar or a string representing the contents of an html table
	def analize(str, stack=["$", grammar.getInitial])
		out = "<tr><td>"

		if str.size == stack.size && stack.size == 0
			return out + "</td><td></td><td>Accept</td></tr>"
		elsif stack.size == 0
			return false
		else
			popped = stack.pop
			action = getAction popped, str[0]

			stack.each{|e| out += "#{e} "} #Prints stack
			out += "#{popped}</td><td>"
			str.each{|e| out += "#{e} "} #Prints string
			out += "</td><td>"

			if popped == str[0]
				out += "pop</td></tr>"
				tmp = analize(str[1...str.size], stack)
				return (tmp==false ? false : out+=tmp)
			elsif action == nil
				return false
			elsif action.size > 1
				return -1
			else
				action = action[0]
				action[0].each{|e| out += " #{e}"} #Prints actions and adds them to the stack
				(0 ... action[0].size).each{|i|
					if action[0][action[0].size-1-i] != Grammar::EPSILON
						stack.push action[0][action[0].size-1-i]
					end
				}
				out += ", #{action[1]}</td></tr>"
				tmp = analize(str, stack)
				return (tmp==false ? false : (tmp==-1 ? -1 : out+tmp))
			end
		end
	end


	def print_table
		@LL_list.each{ |row|
			row.each{ |cell|
				if cell == nil
					print ""
				elsif cell.class == String
					print cell
				else
					cell.each { |entry|
						print "#{entry} "
					}
				end
				print "\t"
			}
			puts ""
		}
	end
	

	private
	

	# Returns smt in this form [[["T", "Ep"], 1], [["T", "Ep"], 1]]
	def getAction(row, column)
		llTableAux = @LL_list.dup
		indexColumn = 0
		indexRow = 0
		
		llTableAux[0].each{|auxColumn|
			if auxColumn == column
				break
			end
			indexColumn = indexColumn+1
		}
		
		llTableAux.each{|auxRow|
			if auxRow[0] == row
				break
			end
			indexRow = indexRow+1
		}
		
		if @LL_list[indexRow] == nil
			return nil
		else
			return @LL_list[indexRow][indexColumn]
		end
	end


	def get_LL_table
		leftSymbols = @grammar.allLeftSymbols.to_a()
		terminalSymbols = @grammar.vt.to_a()
		llTable = Array.new
		
		leftSymbols.push("$")
		terminalSymbols.push("$")
		
		#initial table
		for i in 0..leftSymbols.size
			row = Array.new
			for j in 0..terminalSymbols.size-1
				if i == 0 
					row[j+1] = terminalSymbols[j]
				elsif i>0 && j==0
					row[0] = leftSymbols[i-1]
				end
				
			end
			llTable.push(row)
		end 
	
		rulesList = @grammar.rulesList
		counter = 1
		rulesList.each{|key,values|
			values.each{|value|
				sigma = Array.new
				value.each{|i| sigma.push(i)}
				resultFirst = first(sigma)
				if resultFirst.size==1 && resultFirst.to_a[0]=="" && resultFirst!= nil
					resultFirst = follow(key)
				end
				
				resultFirst.each{|elemntFirst|
					filled = Array.new 
					filled.push(value)
					filled.push(counter)
					llTable = inserInTable(llTable,key,elemntFirst,filled)
				}
				counter+=1
			}
		}
		return llTable
	end 
	

	def inserInTable(table,atRow,atColumn,value)
		
		header = table[0];
		indexHeader = 0
		header.each{|elemnt| 
			if elemnt == atColumn
				break
			end
			indexHeader = indexHeader+1
		}
				
		indexColumn = 0
		table.each{|i|
			if i[0] == atRow
				break
			end
			indexColumn = indexColumn+1
		}

		if table[indexColumn][indexHeader] == nil
			table[indexColumn][indexHeader] = []
		end

		table[indexColumn][indexHeader] << value
		return table
	end
	

	def first(sigma) #sigma is an Array e.g ["Ep","Tp"]
		result = Set.new
		if sigma.size==0
			result.add("")
		elsif sigma[0]==""
			sigma.delete("")
			result.merge first(sigma)
		elsif @grammar.isTerminal? sigma[0]
			result.add(sigma[0])
		elsif @grammar.isNotTerminal? sigma[0]
			alphas = @grammar.getProductions(sigma[0])	
			alphas.each{|alpha|
				newAlpha = alpha.dup
				if sigma[1] != nil
				    newAlpha.push(sigma[1])
				else
					newAlpha.push("")
				end
				result.merge first(newAlpha)
				
			}
		end
		return result
	end 
	

	def follow(rule)
		total = Set.new
		if(@list.include?(rule) == false)
			@list << rule
		end 
		result = Hash.new
		left = @grammar.rulesList.keys
		if(left[0].eql? rule)
			total << "$"
		end 
		
		@grammar.rulesList.each{|key,value|  #Rules witch contain "rule" on the right side
			value.each{|rules| 
				if(rules.include? rule)
					aux = Array.new
					aux << rules
					if(result.has_key?(key))
						auxArray = result[key]
						auxArray.each{|symb| aux << symb}
					end
					result[key] = aux
				end
			}
		}
		
		result.each{|key,value|  
			value.each{|rules| 
				if(rules.include? rule)
					if((rules.size)-1 == rules.index(rule))
						if(@list.include?(key) == false)
							total = total | follow(key)
						end
					end
					
					if(rules.index(rule) < (rules.size)-1)
						aux = first(rules.drop(rules.index(rule)+1))
						aux = aux.to_a
						if(aux.include? "")
							aux.delete_at(aux.index(""))
							total = total | follow(key)
						end
						aux = aux.to_set
						total = total | aux
						
					end
				end
			}
		}
		return total
	end

end