require './Libraries/LexicalAnalizer/RE_Lexical_Analizer'
require './Libraries/Assets/Thing'

class RE_SyntacticAnalizer

	def initialize(expression)
		@expression = expression
		@lexer = RE_Lexical_Analizer.new expression
	end

	attr_reader :expression

	# Resets what is needed in order to analyze another expression
	def change_expression(expression)
		@lexer.change_string expression
		@expression = expression
	end


	def analize(string)
		result = Thing.new
		if E result
			analizer = AnalizerNFA.new(result.value,0)
			return analizer.analizeSimply(string) == string.to_s.size #Cadena valida o no
		else 
			return -1 # Expresion NO valida
		end
	end

	def create_NFA
		result = Thing.new
		if E result
			return result.value
		else
			return -1 # Non valid regular expression
		end
	end

	private

	def E(thing)
	    if T(thing)
		   if Ep(thing)
		       return true
		   end
		end
	    return false
	end


	def Ep(thing)
	     token = @lexer.next_token
	     aux = Thing.new 
	    if token == RE_Tokens::UNION
		     if T(aux)
				thing.value.union(aux.value)
				if Ep(thing)
					return true
				end
		     end
			return false
		end
		@lexer.rewind  #Epsilon
		return true
	end 
	

	def T(thing)
	    if C(thing)
		   if Tp(thing)
		       return true
		   end
		end
	    return false
	end
	

	def Tp(thing)
	     token = @lexer.next_token
	     aux = Thing.new 
	    if token == RE_Tokens::CONCAT
		     if C(aux)
				thing.value.concatenate(aux.value)
				if Tp(thing)
					return true
				end
		     end
			return false
		end
		@lexer.rewind  #Epsilon
		return true
	end
	

	def C(thing)
	    if F(thing)
		   if Cp(thing)
		       return true
		   end
		end
	    return false
	end


	def Cp(thing)
	     token = @lexer.next_token
	     aux = Thing.new 

	    case token
			when RE_Tokens::P_CLOSURE
				thing.value.positive_closure
				if Cp(thing)
					return true
				end
			when RE_Tokens::K_CLOSURE
				thing.value.kleen_closure
				if Cp(thing)
					return true
				end
			when RE_Tokens::ALTERNATIVE
				thing.value.alternative
				if Cp(thing)
					return true
				end
		end			 
		@lexer.rewind  #Epsilon
		return true
	end
	

	def F(thing)
		token = @lexer.next_token
		aux = Thing.new 	
		case token  
			when RE_Tokens::B_LEFT
				if E(aux)
					thing.value = aux.value
					if  @lexer.next_token == RE_Tokens::B_RIGHT
						return true
					end
				end			
			when RE_Tokens::SYMBOL
			    auxLexer = @lexer.deep_clone				
				if auxLexer.next_token == RE_Tokens::RANGE
					if auxLexer.next_token == RE_Tokens::SYMBOL
						thing.value = automatonRange(@lexer.current_lexeme,auxLexer.current_lexeme)
						@lexer = auxLexer
						return true
					end
				else
					thing.value = NFA.new(@lexer.current_lexeme,"general")
					return true
				end
		end
		return false
	end
	

	#function to generate the range automaton, using two parameter lowLimit and upLimit
	def automatonRange(range1,range2)
	    lowLimit = range1.getbyte(0)
		upLimit = range2.getbyte(0)
		
		automaton = NFA.new(lowLimit.chr,"")
		
		while lowLimit <= upLimit-1 do
			lowLimit = lowLimit+1
			automaton.union NFA.new(lowLimit.chr,"")	
        end
		
		return automaton
	end
end
