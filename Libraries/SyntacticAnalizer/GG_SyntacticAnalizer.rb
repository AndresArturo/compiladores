require './Libraries/LexicalAnalizer/Grammar_Lexical_Analizer'
require './Libraries/Assets/Grammar'
require './Libraries/Assets/Thing'

class GG_SyntacticAnalizer

	def initialize(expression)
		@expression = expression
		@lexer = Grammar_Lexical_Analizer.new expression
	end

	attr_reader :expression

	# Resets what is needed in order to analize another expression
	def change_expression(expression)
		@lexer.change_string expression
		@expression = expression
	end

	def analize
		result = Hash.new
	
		if G(result)
			#result.each{|key,value|  
			#		puts key
			#		value.each{|rules| print rules," "} 
			#		puts " "
			#		puts " "
			#}

			grammar = Grammar.new(result)
			return grammar
		else 
			#puts "Verify your grammar"
			return false # Expresion NO valida
		end
	end
	
	private

	def G(hashRules)
	    if RulesList(hashRules)
		   return true
		end
	    return false
	end

	def RulesList(hashRules)
		if Rule(hashRules)
			token = @lexer.next_token
			if token == Grammar_Tokens::DELIMITER
				if MoreRules(hashRules)
					return true
				end
			end
		end
		return false
	end
					
	def MoreRules(hashRules)
	    if RulesList(hashRules)
			return true
		end
		@lexer.rewind  #Epsilon
	    return true
	end
	
	def Rule(hashRules) #rulesList
		leftSymbol = Thing.new
	    if Left(leftSymbol)
			token = @lexer.next_token
			if token == Grammar_Tokens::RULE
				rightList = Array.new
				if RightList(rightList)
					if(hashRules.has_key?(leftSymbol.value))
							auxArray = hashRules[leftSymbol.value];
							auxArray.each{|aux| rightList.push(aux)}
					end
					hashRules[leftSymbol.value] = rightList
					return true
				end
		    end
		end
		return false
	end

	def RightList(rightList) 
		firstRight = Array.new
		if Right(firstRight)
			rightList.push(firstRight)
			if MoreRights(rightList)		
				return true
		    end
		end
	    return false
	end

	def MoreRights(moreRights)
	    token = @lexer.next_token 
		if token == Grammar_Tokens::OR
			if RightList(moreRights)
				return true
			end
		end
		@lexer.rewind  #Epsilon
	    return true
	end
	
	def Right(rightList)
		token = @lexer.next_token
	    if token == Grammar_Tokens::SYMBOL
			rightList.push(@lexer.current_lexeme)
			auxMoreSymbols = Array.new 
			if MoreSymbols(auxMoreSymbols)
				auxMoreSymbols.each{|symbol| rightList.push(symbol)}
				return true
			end
		end
	    return false
	end

	def MoreSymbols(moreSymbols)
	    token = @lexer.next_token
		if token == Grammar_Tokens::SYMBOL
			moreSymbols.push(@lexer.current_lexeme)
			if MoreSymbols(moreSymbols)
				return true
			end
		end
		@lexer.rewind  #Epsilon
	    return true
	end
	
	def Left(symbol)
	    token = @lexer.next_token
		if token == Grammar_Tokens::SYMBOL
			symbol.value = @lexer.current_lexeme
			return true
		end
	    return false
	end
end