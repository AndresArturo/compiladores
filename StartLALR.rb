require './Libraries/SyntacticAnalizer/LR1_Analizer'
require './Libraries/SyntacticAnalizer/RE_SyntacticAnalizer'

#Reads the file containing the grammar
text_grammar = ""
IO.readlines(ARGV[0]).each{|line| text_grammar+=line}
grammar = GG_SyntacticAnalizer.new(text_grammar).analize

#Creates the lexical analyzer asking for regular expressions
token = 0
sets = Array.new 
simplify = Array.new
tmp_array = Array.new
la = LexicalAnalizer.new "",""
grammar.vt.each{|s|
	tmp_array << s
	print "Regular expression for '#{s}':  "
	la.addAnalizer AnalizerNFA.new(RE_SyntacticAnalizer.new(STDIN.gets).create_NFA, token)
	token += 1
}

lalr = LR1_Analizer.new(grammar)
lalr_result = lalr.result
lalr_result.each{|x|
		sets.push(lalr.new_Set(x))}
simplify = lalr.simplify_sets(sets)
lalr.new_table(simplify)

begin 
	print "\nString to analyse: "
	str = STDIN.gets.chomp()
	puts
	la.change_string str

	#Lexically analyzes the input string
	string = Array.new
	while (token=la.next_token)
		if token==-1
			puts "Lexically erroneous string"
			exit
		else
			string << tmp_array[token]
		end
	end
	string << "$"
	
	#LALR(1) analysis 
	lalr_res = lalr.verify_string(["0"],string)

	if lalr_res==false
		puts "Syntacticly erroneous string"
	else
		out = File.new("lalr.html", "w")
		out.puts("<!DOCTYPE html>", "<html>", "<head>",
			"<title>LALR syntactical analyzer</title>",
			"<link rel='stylesheet' href='table.css' type='text/css'/>",
			"</head>", "<body>", "<table>", "<tr>",
			"<th>Stack</th>","<th>String</th>","<th>Action</th>",
			"</tr>",lalr_res,"</table>","</body>","</html>")
		out.close
		puts "Accepted string, generated: lalr.html"
	end

	print "Continue? (y/n): "
	opc = STDIN.gets.chomp()
end while opc=="y"